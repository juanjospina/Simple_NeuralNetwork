# This example is using MNIST handwritten digits. The dataset contains
# 60,000 examples for training and 10,000 examples for testing. The digits have
# been size-normalized and centered in a fixed-size image (28x28 pixels) with
# values from 0 to 1. For simplicity, each image has been flattened and
#  converted to a 1-D numpy array of 784 features (28*28).

from __future__ import print_function
import numpy as np

# Import MNIST data
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("/tmp/data/", one_hot = True)

import tensorflow as tf

# Logging path for tensorboard
logs_path = '/tmp/tensorflow_logs/example/'

# Parameters
learning_rate = 0.001
num_steps = 20000
batch_size = 128
display_step = 1000

# NN Parameters
num_inputs = 784 # MNIST data input (img:shape = 28*28)
n_hidden_1 = 256 # 1st hidden layer
n_hidden_2 = 256 # 2nd hidden layer
num_classes = 10 # Ouput layer with 10 classes (0-9 digits)

# tf Graph input and output
X = tf.placeholder("float", [None, num_inputs])
Y = tf.placeholder("float", [None, num_classes])


# Store layers weight & bias# Store
weights = {
    'h1': tf.Variable(tf.random_normal([num_inputs, n_hidden_1])),
    'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
    'out': tf.Variable(tf.random_normal([n_hidden_2, num_classes]))
}
biases = {
    'b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'b2': tf.Variable(tf.random_normal([n_hidden_2])),
    'out': tf.Variable(tf.random_normal([num_classes]))
}


# --- Create the NN model ---
def neural_net(x):
    # Hidden fully connected layer with 256 neurons
    layer_1 = tf.nn.relu(tf.add(tf.matmul(x, weights['h1']), biases['b1']))
    # Hidden fully connected layer 2 with 256 neurons
    layer_2 = tf.nn.relu(tf.add(tf.matmul(layer_1, weights['h2']), biases['b2']))
    # Ouput fully connected layer with a neuron in each class
    out_layer = tf.matmul(layer_2, weights['out']) + biases['out']

    return out_layer


# --- Construct Model ---
model_scores = neural_net(X)

# Define loss and optimizer
# tf.nn.sigmoid_cross_entropy_with_logits (multilabel classification)
# tf.nn.softmax_cross_entropy_with_logits (multiclass classification)
with tf.name_scope('Model'):
    pred = tf.nn.softmax_cross_entropy_with_logits(logits=model_scores, labels=Y)
with tf.name_scope('Loss'):
    loss_op = tf.reduce_mean(pred)

#optimizer = tf.train.AdagradOptimizer(learning_rate = learning_rate)
#optimizer = tf.train.GradientDescentOptimizer(learning_rate = learning_rate)
optimizer = tf.train.AdamOptimizer(learning_rate = learning_rate)
with tf.name_scope('Optimizer'):
    train_op = optimizer.minimize(loss_op)

# Predictions made by neural network while training
train_prediction = tf.nn.softmax(model_scores)

# Function Name:  accuracy
# Description: Evaluates the accuracy of the model
# Arguments: predictions - values given by trained NN
#           labels - real values to check on
def accuracy(predictions, labels):
    preds_correct_boolean = np.argmax(predictions,1) == np.argmax(labels,1)
    correct_predictions = np.sum(preds_correct_boolean)
    accuracy = 100.0 * correct_predictions/predictions.shape[0]
    return accuracy


# Get the test values predicted by the neural network and the real test values
test_prediction = tf.nn.softmax(neural_net(mnist.test.images))
test_labels = mnist.test.labels

# ----- Start Training the Model -----

# --- Create a summary to monitor cost tensor ---
tf.summary.scalar("loss", loss_op)
# Merge all summaries into a single op
merged_summary_op = tf.summary.merge_all()

# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()

# ----- Start Training -----
with tf.Session() as sess:

    # op to write logs to Tensorboard
    summary_writer = tf.summary.FileWriter(logs_path, graph=sess.graph)
    writer_2 = tf.summary.FileWriter(logs_path, graph=sess.graph)

    # Run initializer
    sess.run(init)

    for step in range(num_steps+1):
        # Create the batches
        batch_x, batch_y = mnist.train.next_batch(batch_size)

        # Run optimization operation (backpropagation)
        _, loss, predictions = sess.run([train_op, loss_op, train_prediction], feed_dict={X: batch_x, Y: batch_y})

        if step % 1000 == 0:
            # Write loss to tensorboard
            summary_str = sess.run(merged_summary_op, feed_dict={X: batch_x, Y: batch_y})
            writer_2.add_summary(summary_str, step)
            writer_2.flush()

            print("Step " + str(step) + ", Minibatch Loss= " + \
                "{:.4f}".format(loss))


    print("Optimization Finished!")

    # The test_prediction.eval() evaluates the test_prediction data into the trained neural network
    print ("Test accuracy in %: " "{:.4f}".format(accuracy(test_prediction.eval(), test_labels)))

    print("Run the command line:\n" \
          "--> tensorboard --logdir=/tmp/tensorflow_logs " \
          "\nThen open http://0.0.0.0:6006/ into your web browser")
