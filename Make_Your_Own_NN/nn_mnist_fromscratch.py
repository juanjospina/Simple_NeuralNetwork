# Code for 3-layer Neural Network
# Coded from scratch using only numpy and scipy

import numpy as np
import math
import scipy.special # for the sigmoid function expit()
import matplotlib.pyplot # plotting arrays
#%matplotlib inline # plots inside the notbeook not other window


# Neural Network Class
class NeuralNetwork():
    # Initialize the neural network
    def __init__(self, num_inputs, num_hidden, num_outputs, learning_rate):
        # Set the number of the nodes in each layer
        self.input_nodes = num_inputs
        self.hidden_nodes = num_hidden
        self.output_nodes = num_outputs

        # Link the weight matrices - wih and who
        # Weights inside the arrays are w_i_j, where link is from node i to
        # ndoe j in the next layer
        self.wih = np.random.normal(0.0, pow(self.hidden_nodes, -0.5), (self.hidden_nodes, self.input_nodes))
        self.who = np.random.normal(0.0, pow(self.output_nodes, -0.5), (self.output_nodes, self.hidden_nodes))

        # learning_rate
        self.lr = learning_rate

        # activation function is the sigmoid function
        self.activation_function = lambda x: scipy.special.expit(x)


        pass


    # Function to train the neural network
    def train(self, input_list, target_list):
        # Convert the input_list to 2D array
        inputs = np.array(input_list, ndmin=2).T
        targets = np.array(target_list, ndmin=2).T

        # Calculate signals into hidden layer
        hidden_inputs = np.dot(self.wih, inputs)
        # Calculate the signals emerging from hidden layer
        hidden_outputs = self.activation_function(hidden_inputs)

        # Calculate signals into final output layer
        final_inputs = np.dot(self.who, hidden_outputs)
        # Calculate the signals emerging from final output layer
        final_outputs = self.activation_function(final_inputs)

        # Output layer error is the (target-actual)
        output_errors = targets - final_outputs
        # Hidden layer error is the output_errors, split by weights,
        # recombined at hidden nodes
        hidden_errors = np.dot(self.who.T, output_errors)

        # Update the weights for the link for the links between the hidden
        # and output layers
        self.who += self.lr * np.dot((output_errors * final_outputs * (1 - final_outputs)), np.transpose(hidden_outputs))

        # Update the weights for the links between the input and hidden layers
        self.wih += self.lr * np.dot((hidden_errors * hidden_outputs * (1 - hidden_outputs)), np.transpose(inputs))

        pass


    # Function that Querys the Neural Network
    def query(self, input_list):
        # convert input_list to 2d array
        inputs = np.array(input_list, ndmin=2).T

        # calculate signals into hidden layer
        hidden_inputs = np.dot(self.wih, inputs)
        # calculate the signals emerging from hidden layer
        hidden_outputs = self.activation_function(hidden_inputs)

        # calculate signals into final output layer
        final_inputs = np.dot(self.who, hidden_outputs)
        # calculate the signals emerging from final output layer
        final_outputs = self.activation_function(final_inputs)

        return final_outputs


     # backquery the neural network
    # we'll use the same termnimology to each item,
    # eg target are the values at the right of the network, albeit used as input
    # eg hidden_output is the signal to the right of the middle nodes
    def backquery(self, targets_list):
        # transpose the targets list to a vertical array
        final_outputs = numpy.array(targets_list, ndmin=2).T

        # calculate the signal into the final output layer
        final_inputs = self.inverse_activation_function(final_outputs)

        # calculate the signal out of the hidden layer
        hidden_outputs = numpy.dot(self.who.T, final_inputs)
        # scale them back to 0.01 to .99
        hidden_outputs -= numpy.min(hidden_outputs)
        hidden_outputs /= numpy.max(hidden_outputs)
        hidden_outputs *= 0.98
        hidden_outputs += 0.01

        # calculate the signal into the hidden layer
        hidden_inputs = self.inverse_activation_function(hidden_outputs)

        # calculate the signal out of the input layer
        inputs = numpy.dot(self.wih.T, hidden_inputs)
        # scale them back to 0.01 to .99
        inputs -= numpy.min(inputs)
        inputs /= numpy.max(inputs)
        inputs *= 0.98
        inputs += 0.01

        return inputs



    # # Defined maehtemtically the sigmoid function
    # def sigmoid(x):
    #     return 1 / (1 + math.exp(-x))


#---------------------------------------------------------------------
# Main Code calling the NN and using it for MNIST data
#---------------------------------------------------------------------

# Number of input, hidden and outputs neurons
input_neurons = 784
hidden_neurons = 200
output_neurons = 10

# learning rate
learningrate = 0.1

# Create instance of the neural network
nn = NeuralNetwork(input_neurons, hidden_neurons, output_neurons, learningrate)

# Load the MNIST training data from the CSV file
training_data_file = open("./Make_Your_Own_NN/mnist_train.csv", 'r')
training_data_list = training_data_file.readlines()
training_data_file.close()

# Train the neural network

# Epochs is the the number of times the (entire) training dataset is used for training
epochs = 5

for e in range(epochs):
    # Go through all records in the training dataset
    for record in training_data_list:

        # Split the record by the ',' commas
        all_values = record.split(',')

        # Scale and shift the inputs
        inputs = (np.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01

        # Create the target output values (all 0.01, except the desired label which is 0.99)
        targets = np.zeros(output_neurons) + 0.01

        # all_values[0] is the target label for this record
        targets[int(all_values[0])] = 0.99

        # Train the NN
        nn.train(inputs, targets)
        pass
    pass




#------------------------------------------------------
# TEST the Neural network
#------------------------------------------------------

# Load the MNIST testing data from CSV file
test_data_file = open("./Make_Your_Own_NN/mnist_test.csv", 'r')
test_data_list = test_data_file.readlines()
test_data_file.close()

# scorecard to store how well the network performs
scorecard = []

# Go through all the records in the test dataset
for record in test_data_list:

    # Split the record by the ',' commas
    all_values = record.split(',')

    # Correct answer is the first value
    correct_label = int(all_values[0])\

    # Scale and shift the inputs
    inputs = (np.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01

    # Query the network
    outputs = nn.query(inputs)

    # The index with the highest value corresponds to the label
    label = np.argmax(outputs)

    # Append the correct or incorrect to list
    if (label == correct_label):
        scorecard.append(1)
    else:
        scorecard.append(0)
        pass

    pass

# Calculate the performance of the score, the fraction of correct answers
scorecard_array = np.asarray(scorecard)
print("performance = ", scorecard_array.sum()/scorecard_array.size)


# --------------------------------------------------------------------
# Backward Query
# -------------------------------------------------------------------
# run the network backwards, given a label, see what image it produces

# # label to test
# label = 0
# # create the output signals for this label
# targets = numpy.zeros(output_nodes) + 0.01
# # all_values[0] is the target label for this record
# targets[label] = 0.99
# print(targets)
#
# # get image data
# image_data = nn.backquery(targets)
#
# # plot image data
# matplotlib.pyplot.imshow(image_data.reshape(28,28), cmap='Greys', interpolation='None')
